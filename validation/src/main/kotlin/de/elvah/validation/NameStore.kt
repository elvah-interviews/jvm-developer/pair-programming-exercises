package de.elvah.validation

import org.springframework.stereotype.Component


/**
 * Dumb class that stores strings
 */
@Component
class NameStore {

    fun contains(name: String): Boolean {
        return names.any { it.lowercase() == name.lowercase().trim() }
    }

    companion object {
        private val names = listOf(
            "Barbara",
            "Nils",
            "Kevin",
            "Martin",
            "Denis",
            "Marvin"
        )
    }

}