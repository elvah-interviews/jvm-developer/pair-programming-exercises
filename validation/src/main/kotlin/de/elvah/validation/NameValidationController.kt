package de.elvah.validation

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class NameValidationController {

    @PostMapping("/validate")
    fun validate(request: Request) {

        // do nothing
    }


    data class Request(
        val names: List<String>
    )
}