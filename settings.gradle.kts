rootProject.name = "exercises"

include("refactoring")
project(":refactoring").projectDir = File("refactoring")

include("validation")
project(":validation").projectDir = File("validation")