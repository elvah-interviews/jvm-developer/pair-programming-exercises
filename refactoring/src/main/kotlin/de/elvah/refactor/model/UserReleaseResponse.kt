package de.elvah.refactor.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.Instant

data class UserReleaseResponse(
    @JsonProperty("id")
    val id: String,

    @JsonProperty("name")
    val name: String,

    @JsonProperty("createdAt")
    val createdAt: Instant
)