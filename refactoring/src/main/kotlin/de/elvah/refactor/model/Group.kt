package de.elvah.refactor.model

import java.time.Instant

/**
 * Example Data Class
 */
data class Group(
    val id: String,
    val createdAt: Instant
)
