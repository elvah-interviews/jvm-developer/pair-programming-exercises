package de.elvah.refactor.model

import java.time.Instant

/**
 * Example Data Class
 */
data class User(
    val id: String,
    val name: String,
    val createdAt: Instant
)