package de.elvah.refactor.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.Instant

data class GroupResponse(
    @JsonProperty("id")
    val id: String,
    @JsonProperty("createdAt")
    val createdAt: Instant
)
