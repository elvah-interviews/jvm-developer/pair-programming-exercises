package de.elvah.refactor.repository

import de.elvah.refactor.model.User
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*

/**
 * Dummy Repo no need to refactor its internals
 */
@Component
class UserRepository {


    fun findByIdOrNull(userId: String): User? {
        return User(
            id = UUID.randomUUID().toString(),
            name = "Lysam Pytos",
            createdAt = Instant.now()
        )
    }
}