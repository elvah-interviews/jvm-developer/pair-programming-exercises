package de.elvah.refactor.repository

import de.elvah.refactor.model.Group
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*

/**
 * Dummy Repo no need to refactor its internals
 */
@Component
class GroupRepository {


    fun findByUserId(userId: String): List<Group> {
        return listOf(
            group(),
            group(),
            group(),
            group(),
            group(),
        )
    }

    /**
     * helper function not part of the refacoring challenge
     */
    private fun group() = Group(
        id = UUID.randomUUID().toString(),
        createdAt = Instant.now()
    )

}