package de.elvah.refactor.repository

import de.elvah.refactor.model.Release
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*

/**
 * Dummy Repo no need to refactor its internals
 */
@Component
class ReleaseRepository {

    fun findByUserId(userId: String): List<Release> {
        return listOf(
            release(),
            release(),
            release(),
            release(),
            release(),
        )
    }

    private fun release() = Release(
        id = UUID.randomUUID().toString(),
        name = "Release ${UUID.randomUUID()}",
        createdAt = Instant.now()
    )

}