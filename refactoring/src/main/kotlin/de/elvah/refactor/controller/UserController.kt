package de.elvah.refactor.controller

import de.elvah.refactor.model.GroupResponse
import de.elvah.refactor.model.UserReleaseResponse
import de.elvah.refactor.model.UserResponse
import de.elvah.refactor.repository.GroupRepository
import de.elvah.refactor.repository.ReleaseRepository
import de.elvah.refactor.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@RestController
class UserController {

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var groupRepository: GroupRepository

    @Autowired
    lateinit var releaseRepository: ReleaseRepository

    @GetMapping("/users/{userId}")
    fun user(@PathVariable userId: String): UserResponse {
        return userRepository.findByIdOrNull(userId)?.let {
            UserResponse(
                id = it.id,
                name = it.name,
                createdAt = it.createdAt
            )
        } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

    @GetMapping("/users/{userId}/groups")
    fun userGroups(@PathVariable userId: String): List<GroupResponse> {
        return groupRepository.findByUserId(userId).map {
            GroupResponse(
                id = it.id,
                createdAt = it.createdAt
            )
        }
    }

    @GetMapping("/users/{userId}/releases")
    fun userReleases(@PathVariable userId: String): List<UserReleaseResponse> {
        return releaseRepository.findByUserId(userId).map {
            UserReleaseResponse(
                id = it.id,
                name = it.name,
                createdAt = it.createdAt
            )
        }
    }
}