package de.elvah.refactor

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ElvahRefactoringChallengeApplication

fun main(args: Array<String>) {
    runApplication<ElvahRefactoringChallengeApplication>(*args)
}
